
# coding: utf-8

import numpy as np
import pandas as pd

import os
import json
from datetime import datetime, timedelta

from Script1_download_files import download_blob_files
from Script2_prod_data_transformation import data_transformation
from Script3a_prepare_base_schema import prepare_base_schema
from script3b_mapping_datasets import map_data
from Script4_upload_files_to_blob import upload_to_blob
import logging
# logger = logging.getLogger(__name__)

from azure.storage.blob import BlockBlobService



def get_collated_from_blob(container_name, blob_account_name, blob_account_key):
    """This function will fetch the collated files from container if available.
    """
    try:# read config file
        logging.info('Trying to download collated files from blob of container :{}'.format(container_name))

        block_blob_service = BlockBlobService(blob_account_name, blob_account_key)
        
        os.makedirs(container_name, exist_ok=True)
        files_list = []
        generator = block_blob_service.list_blobs(container_name)
        for blob in generator:
            files_list.append(blob.name)
        for el in files_list:
            try:
                local_file_name = el
                full_path_to_file = './'+container_name+'/'+el

                # if file already exists do not download it again
                if os.path.isfile(full_path_to_file):
                    continue
                block_blob_service.get_blob_to_path(container_name, local_file_name, full_path_to_file)
            except Exception as e:
                logging.warning("get_collated_from_blob")
                logging.warning(e)
        if os.path.isfile('./'+container_name+'/modelling_dataset.csv'):
            logging.info('Successfully downloaded collated files from blob of container :{}'.format(container_name))
            return True
        else:
            logging.info('Failed to download collated files from blob of container :{}'.format(container_name))
            return False

    except Exception as e:
        logging.error("get_collated_from_blob")
        logging.error(e)
        return False


def prepare_final_dataset():
    """
    This script will prepare the data set for modelling of the last 30 days.
    """
    try:
        logging.info("Running : script 5")
        with open("./config.json", "r") as f_pt:
            config = json.load(f_pt)

        # instantiate blob obj
        blob_account_name = config['blob_account_name']
        blob_account_key = config['blob_account_key']

        # get the list of last 30 days folder excluding current day
        today_date = (datetime.now()-timedelta(days=1)).date().strftime('%Y-%m-%d')
        # date_list = [str(x).replace('-', '') for x in (np.array(today_date, dtype=np.datetime64)-np.arange(30))]
        date_list = (np.array(today_date, dtype=np.datetime64)-np.arange(30))
        date_list = [str(x).replace('-', '') for x in sorted(date_list, reverse=True)]
        # print(date_list)

        # to store all the modelling dataset of the last 30 days
        dataset_list = []

        i = 0
        while i<len(date_list):
            file_path = './collatedfiles'+date_list[i]+'/modelling_dataset.csv'
            if os.path.isfile(file_path):
                logging.info("Found modelling_dataset of date :{}".format(date_list[i]))
                dataset_list.append(pd.read_csv(file_path).copy())
                i += 1
            # add get_collated_from_blob
            elif get_collated_from_blob('collatedfiles'+date_list[i], blob_account_name, blob_account_key):
                continue
            else:
                # i+=1;continue  # remove
                logging.info("Not found modelling_dataset of date :{}".format(date_list[i]))
                date_obj = datetime.strptime(date_list[i], '%Y%m%d').date()
                logging.info(date_obj)
                
                # Script1_download_files
                script1_resp = download_blob_files(date_obj)
                if script1_resp==100:
                    logging.warning("No data found of date :{}".format(date_list[i]))
                    # create empty modelling_data.csv file in collated files folder
                    empty_data = """module_position,head_id,start_dt,end_dt,Event,8000E606_errorline,80003704_errorline,80000706_errorline,80810000_errorline,95210000_errorline,8000060B_errorline,80002C01_errorline,80000F01_errorline,8000390B_errorline,80005605_errorline,80000758_errorline,80000776_errorline,80000751_errorline,8D3D0000_errorline,80000F02_errorline,80001B13_errorline,80000705_errorline,80001B09_errorline,80007F15_errorline,80007F16_errorline,80000775_errorline,80003709_errorline,80000707_errorline,8000070F_errorline,80003502_errorline,80001B0D_errorline,8000202F_errorline,80004501_errorline,8000180B_errorline,80001B06_errorline,80004511_errorline,8F550000_errorline,80001907_errorline,8EB60000_errorline,8000071B_errorline,8000071C_errorline,80006813_errorline,80000902_errorline,80003A0D_errorline,80003A31_errorline,00000000_errorline,80000760_errorline,80006501_errorline,80003A19_errorline,80003A1A_errorline,8000680B_errorline,91240000_errorline,8D350000_errorline,8D390000_errorline,80001913_errorline,8000070E_errorline,80001904_errorline,8000190B_errorline,80001910_errorline,80004F04_errorline,80009501_errorline,87B20000_errorline,80001C01_errorline,91450000_errorline,80003712_errorline,87E60000_errorline,87E70000_errorline,88E00000_errorline,93240000_errorline,80003718_errorline,87B30000_errorline,8F590000_errorline,8000190C_errorline,93A20000_errorline,8CC40000_errorline,8000D721_partdataerror,8000340E_partdataerror,80003417_partdataerror,8000340F_partdataerror,8000D720_partdataerror,80003410_partdataerror,8000D701_partdataerror,8000D747_partdataerror,8000D744_partdataerror,8000D741_partdataerror,8000D722_partdataerror,8000D727_partdataerror,8000193D_partdataerror,8000D732_partdataerror,8000D730_partdataerror,8000D749_partdataerror,8000D760_partdataerror,8000D743_partdataerror,80003411_partdataerror,8000D731_partdataerror,8000D702_partdataerror,8000D748_partdataerror,8000D703_partdataerror,8000D721_partdataerror_holder,8000D701_partdataerror_holder,80003410_partdataerror_holder,8000340E_partdataerror_holder,8000D720_partdataerror_holder,8000D747_partdataerror_holder,80003417_partdataerror_holder,8000D727_partdataerror_holder,8000D744_partdataerror_holder,8000340F_partdataerror_holder,8000D730_partdataerror_holder,8000D722_partdataerror_holder,8000D760_partdataerror_holder,8000D741_partdataerror_holder,8000193D_partdataerror_holder,8000D749_partdataerror_holder,80003411_partdataerror_holder,8000D732_partdataerror_holder,8000D748_partdataerror_holder,8000D743_partdataerror_holder,8000D702_partdataerror_holder,8000D731_partdataerror_holder,8000D703_partdataerror_holder,8000E606_partsoutstopage,80003704_partsoutstopage,80000F01_partsoutstopage,80000706_partsoutstopage,80000705_partsoutstopage,80000776_partsoutstopage,80000758_partsoutstopage,80003709_partsoutstopage,8D3D0000_partsoutstopage,95210000_partsoutstopage,8000180B_partsoutstopage,80005605_partsoutstopage,80001B13_partsoutstopage,80810000_partsoutstopage,8000071B_partsoutstopage,8000060B_partsoutstopage,80002C01_partsoutstopage,8000202F_partsoutstopage,93A20000_partsoutstopage,8000190C_partsoutstopage,80004501_partsoutstopage,8CC40000_partsoutstopage,80001B09_partsoutstopage,80003502_partsoutstopage,8000390B_partsoutstopage,80001904_partsoutstopage,80000707_partsoutstopage,80000751_partsoutstopage,91240000_partsoutstopage,80001B0D_partsoutstopage,80003A19_partsoutstopage,80003A1A_partsoutstopage,80003A0D_partsoutstopage,80000F02_partsoutstopage,80001907_partsoutstopage,8000070F_partsoutstopage,80001910_partsoutstopage,8000190B_partsoutstopage,8000070E_partsoutstopage,80003A31_partsoutstopage,87B20000_partsoutstopage,80006501_partsoutstopage,91450000_partsoutstopage,8EB60000_partsoutstopage,80004F04_partsoutstopage,8000680B_partsoutstopage,80006813_partsoutstopage,80000775_partsoutstopage,88E00000_partsoutstopage,87E60000_partsoutstopage,87E70000_partsoutstopage,93240000_partsoutstopage,00000000_partsoutstopage,8F590000_partsoutstopage,80000902_partsoutstopage,80001C01_partsoutstopage,80001B06_partsoutstopage,80004511_partsoutstopage,8F550000_partsoutstopage,8000071C_partsoutstopage,80007F16_partsoutstopage,80007F15_partsoutstopage,80000760_partsoutstopage,80001913_partsoutstopage,80009501_partsoutstopage,8D390000_partsoutstopage,87B30000_partsoutstopage,80003718_partsoutstopage,FEEDERSTATUS,PDERROR,PARTSOUTERROR2,ALAR\n"""
                    os.makedirs('./collatedfiles'+date_list[i], exist_ok=True)
                    with open('./collatedfiles'+date_list[i]+'/modelling_dataset.csv', 'w') as f_pt:
                        f_pt.write(empty_data)
                    
                    # Script4_upload_files_to_blob
                    upload_to_blob(date_obj)
                    continue

                # Script2_prod_data_transformation
                data_transformation(date_obj)

                # Script3a_prepare_base_schema
                prepare_base_schema(date_obj)

                # script3b_mapping_datasets
                map_data(date_obj)

                # Script4_upload_files_to_blob
                upload_to_blob(date_obj)

                # break # remove break
                # Pass the date to script 1-5 & then try again
                # fetch it from blob
                # else create modelling_dataset.csv for that date

        logging.info("got '{}' days of data".format(len(dataset_list)))
        # merge all the 30 dfs
        modelling_df = pd.concat(dataset_list, ignore_index=True, sort=False)

        indep_vars = config['indep_vars']

        # create the missing columns(indep vars) in the modelling df
        logging.info("Add independent vars")
        for i in set(indep_vars)-set(modelling_df.columns):
            # logging.critical(i)
            modelling_df[i] = 0

        # replace `NaN` values with Zero
        modelling_df.fillna(0, inplace=True)

        """
        Filter to keep the datapoints of working heads only
        dataset should contain only the latest pair of module_position and head_id
        """
        modelling_df.sort_values(['module_position', 'head_id', 'end_dt'], ascending=[True, True, False], inplace=True)
        modelling_df['event_cumsum'] = 0
        modelling_df['event_cumsum'] = modelling_df.groupby(['module_position', 'head_id']).Event.cumsum()
        # to keep only current working headers data
        modelling_df = modelling_df[modelling_df['event_cumsum'] == 0]

        # keep only the indep vars
        modelling_df = modelling_df.loc[:,indep_vars]

        # save the dataset for processing 
        modelling_df.to_csv('./final_dataset.csv', index=False)

        logging.info("Success : Completed script 5")
        return 0
    except Exception as e:
        logging.error("Failed : script 5")
        logging.error(e)
        return (1, e)
