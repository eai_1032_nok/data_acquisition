
# coding: utf-8

# This script will map data from collated files to the prepared base schema(from script `3a`)    



import numpy as np
import pandas as pd
from pprint import pprint as pp
import re
from datetime import datetime, timedelta, date

import logging
# logger = logging.getLogger(__name__)



def map_data(date_obj):
    """
    date_obj : datetime.date object
    """
    try:
        logging.info("Running : script 3b for {}".format(date_obj))
        # create datetime obj
        assert isinstance(date_obj, date)
        
        # path of collated dir
        collated_file_dir = './collatedfiles'+date_obj.strftime('%Y%m%d')
 
        # Load Base Schema
        head_dataset = pd.read_csv(collated_file_dir+'/base_schema.csv')  # , names=['line_name', 'module_name', 'date_time', 'head_type', 'head_id', 'machine_id']


        # change the datetime columns
        head_dataset['start_dt'] = pd.to_datetime(head_dataset['start_dt'])
        head_dataset['end_dt'] = pd.to_datetime(head_dataset['end_dt'])

        # cycle time - Not Mapped
        cycletime_df = pd.read_csv(collated_file_dir+'/cycle_time_data.csv')


        # Error Files

        # 1. Error Line - Mapped
        logging.info(" mapping Error Line")
        error_info_errorline_collate_df = pd.read_csv(collated_file_dir+'/error_info_errorline_collate.csv')

        # add module_position column
        error_info_errorline_collate_df['module_position'] = [x[0]+'__'+str(x[1])[-1] for x in zip(error_info_errorline_collate_df['Machine'], error_info_errorline_collate_df['Module'])]
        # drop null columns
        error_info_errorline_collate_df.dropna(axis=1, inplace=True, how='all')

        # create error column
        for error in error_info_errorline_collate_df['Error code'].unique():
            head_dataset[error+'_errorline'] = 0

        # map the error count
        error_info_errorline_collate_df_T = error_info_errorline_collate_df.T.to_dict()
        for el in error_info_errorline_collate_df_T.keys():#.iterrows():#.to_dict():
            module_position, error_code, date_time = error_info_errorline_collate_df_T[el]['module_position'], error_info_errorline_collate_df_T[el]['Error code'], datetime.strptime(error_info_errorline_collate_df_T[el]['date_time'], '%Y-%m-%d %H:%M:%S')
            index = head_dataset[(head_dataset['module_position'] == module_position) & (head_dataset['start_dt']<date_time) & (date_time<=head_dataset['end_dt'])].index
            head_dataset.loc[index, error_code+'_errorline'] += 1   # +'_errorline'


        # 2. Error Stopage  - No `Error code` column -Not mapped
        error_info_errorstopage_collate_df = pd.read_csv(collated_file_dir+'/errorinfopartsoutstopage_collate.csv')
    

        # 3. partdataerror  - mapped
        logging.info(" mapping partdataerror")
        errorinfo_partdataerror_df = pd.read_csv(collated_file_dir+'/errorinfo_partdataerror.csv')

        # create module_position
        errorinfo_partdataerror_df['module_position'] = errorinfo_partdataerror_df['Machine']+'__'+errorinfo_partdataerror_df['Position'].apply(lambda x: x[2])
        
        # create error column
        for error in errorinfo_partdataerror_df['Error code'].unique():
            head_dataset[error+'_partdataerror'] = 0
        
        errorinfo_partdataerror_df_T = errorinfo_partdataerror_df.T.to_dict()
        for el in errorinfo_partdataerror_df_T.keys():#.iterrows():#.to_dict():
            module_position, error_code, date_time = errorinfo_partdataerror_df_T[el]['module_position'], errorinfo_partdataerror_df_T[el]['Error code'], datetime.strptime(errorinfo_partdataerror_df_T[el]['date_time'], '%Y-%m-%d %H:%M:%S')
            index = head_dataset[(head_dataset['module_position'] == module_position) & (head_dataset['start_dt']<date_time) & (date_time<=head_dataset['end_dt'])].index
            head_dataset.loc[index, error_code+'_partdataerror'] += 1   # +'_errorline'


        # # 4. Part Data Error feeder  - Not Mapped
        # errorinfo_partdataerror_feeder_df = pd.read_csv(collated_file_dir+'/partdataerror_feeder.csv')
        # print(errorinfo_partdataerror_feeder_df.columns)
        # # add module_position column
        # errorinfo_partdataerror_feeder_df['module_position'] = [x[0]+'__'+str(x[1])[2] for x in zip(errorinfo_partdataerror_feeder_df['Machine'], errorinfo_partdataerror_feeder_df['Position'])]

        # # create error columns
        # for error in errorinfo_partdataerror_feeder_df['Error code'].unique():
        #     head_dataset[error+'_partdataerror'] = 0

        # # map the error count
        # errorinfo_partdataerror_df_T = errorinfo_partdataerror_feeder_df.T.to_dict()
        # for el in errorinfo_partdataerror_df_T.keys():
        #     module_position, error_code, date_time = errorinfo_partdataerror_df_T[el]['module_position'], errorinfo_partdataerror_df_T[el]['Error code'], datetime.strptime(errorinfo_partdataerror_df_T[el]['date_time'], '%Y-%m-%d %H:%M:%S')
        #     index = head_dataset[(head_dataset['module_position'] == module_position) & (head_dataset['start_dt']<date_time) & (date_time<=head_dataset['end_dt'])].index
        #     head_dataset.loc[index, error_code+'_partdataerror'] += 1


        # 5. Part Data Error (holder)  - Mapped
        logging.info(" mapping Part Data Error (holder)")
        errorinfo_partdataerror_holder_collate_df = pd.read_csv(collated_file_dir+'/errorinfo_partdataerror_holder_collate.csv')

        # drop null columns
        errorinfo_partdataerror_holder_collate_df.dropna(axis=1, how='all', inplace=True)

        # add `module_position` column
        errorinfo_partdataerror_holder_collate_df['module_position'] = errorinfo_partdataerror_holder_collate_df['Holder'].apply(lambda x : x.split(' ')[0]+'__'+x.split(' ')[1][1])

        # create error columns
        for error in errorinfo_partdataerror_holder_collate_df['Error code'].unique():
            head_dataset[error+'_partdataerror_holder'] = 0
        # map the error count
        errorinfo_partdataerror_holder_collate_df_T = errorinfo_partdataerror_holder_collate_df.T.to_dict()
        for el in errorinfo_partdataerror_holder_collate_df_T.keys():#.iterrows():#.to_dict():
            module_position, error_code, date_time = errorinfo_partdataerror_holder_collate_df_T[el]['module_position'], errorinfo_partdataerror_holder_collate_df_T[el]['Error code'], datetime.strptime(errorinfo_partdataerror_holder_collate_df_T[el]['date_time'], '%Y-%m-%d %H:%M:%S')
            index = head_dataset[(head_dataset['module_position'] == module_position) & (head_dataset['start_dt']<date_time) & (date_time<=head_dataset['end_dt'])].index
            head_dataset.loc[index, error_code+'_partdataerror_holder'] += 1


        # 6. Part Data Error (nozzle)  -Not mapped 
        # can not assign module position or head id in this dataset

        # errorinfo_partdataerror_nozzle_collate_df = pd.read_csv(collated_file_dir+'/errorinfo_partdataerror_nozzle_collate.csv')
        # print(errorinfo_partdataerror_nozzle_collate_df.shape)
        # errorinfo_partdataerror_nozzle_collate_df.dropna(axis=1, how='all', inplace=True)
        # errorinfo_partdataerror_nozzle_collate_df.dropna(axis=0, how='any', inplace=True)



        # 7. Parts Out Stopage - Mapped
        logging.info(" mapping Parts Out Stopage")
        errorinfopartsoutstopage_collate_df = pd.read_csv(collated_file_dir+'/errorinfopartsoutstopage_collate.csv')
        errorinfopartsoutstopage_collate_df.dropna(axis=1, how='all', inplace=True)
        errorinfopartsoutstopage_collate_df.dropna(axis=0, how='any', inplace=True)

        # create module_position column
        errorinfopartsoutstopage_collate_df['module_position'] = [x[0]+'__'+str(x[1])[2] for x in zip(errorinfopartsoutstopage_collate_df['Machine'], errorinfopartsoutstopage_collate_df['Module'])]

        # create error column
        for error in errorinfopartsoutstopage_collate_df['Error code'].unique():
            head_dataset[error+'_partsoutstopage'] = 0

        # map the error count
        errorinfopartsoutstopage_collate_df_T = errorinfopartsoutstopage_collate_df.T.to_dict()
        for el in errorinfopartsoutstopage_collate_df_T.keys():#.iterrows():#.to_dict():
            module_position, error_code, date_time = errorinfopartsoutstopage_collate_df_T[el]['module_position'], errorinfopartsoutstopage_collate_df_T[el]['Error code'], datetime.strptime(errorinfopartsoutstopage_collate_df_T[el]['date_time'], '%Y-%m-%d %H:%M:%S')
            index = head_dataset[(head_dataset['module_position'] == module_position) & (head_dataset['start_dt']<date_time) & (date_time<=head_dataset['end_dt'])].index
            head_dataset.loc[index, error_code+'_partsoutstopage'] += 1   # +'_errorline'


        # eventlog - Mapped
        logging.info(" mapping Event log")
        eventlog_df = pd.read_csv(collated_file_dir+'/eventlog_collate.csv')
        eventlog_df.dropna(axis=1, how='all', inplace=True)
        eventlog_df.dropna(axis=0, how='any', inplace=True)

        # create `module_position` column to map with `floor_layout_df`
        eventlog_df['module_position'] = [x[0]+'__'+str(x[1])[-1] for x in zip(eventlog_df['Machine'], eventlog_df['Module'])]
        
        columns = ['date_time' if x=='Date & time' else x for x in eventlog_df.columns]
        eventlog_df.columns = columns

        # drop Empty Rows
        eventlog_df.dropna(axis=0, inplace=True, how='any')

        # add the event error columns
        for el in eventlog_df.Event.unique():
            head_dataset[el] = 0

        # map the count of error on the model dataset
        eventlog_T = eventlog_df.T.to_dict()
        for el in eventlog_T.keys():#.iterrows():#.to_dict():
            module_position, event, date_time = eventlog_T[el]['module_position'], eventlog_T[el]['Event'], datetime.strptime(eventlog_T[el]['date_time'], '%Y-%m-%d %H:%M:%S')
        #     print(module_position, event, date_time)
            index = head_dataset[(head_dataset['module_position'] == module_position) & (head_dataset['start_dt']<date_time) & (date_time<=head_dataset['end_dt'])].index
        #     print(el)
            head_dataset.loc[index, event] += 1


        # Panel - Not Mapped 
        # Cannot map it to the Head ID
        # panel_df = pd.read_csv(collated_file_dir+'/panel_files_collation.csv')



        # PartUsageFIDL - Not Mapped 
        # Cannot map it to the Head ID
        # partusage_fidl_df = pd.read_csv(collated_file_dir+'/partusage_fidl_collate.csv')


        # PartUsagePartNumber - Not Mapped 
        # Cannot map it to the Head ID
        # partusage_partnumber_df = pd.read_csv(collated_file_dir+'/partusage_partnumber_collate.csv')


        # PartUsagePosition - Not Mapped 
        # Cannot map it to the Head ID
        # partusage_position_df = pd.read_csv(collated_file_dir+'/partusage_position_collate.csv')


        # summaryprod_partusage - Mapped
        logging.info(" mapping summaryprod_partusage")
        summaryprod_partusage_df = pd.read_csv(collated_file_dir+'/summary_prod_partusage_collate.csv')

        ## Objective : create the column `module_position`

        # `line_name` and `module_number` are `int` type

        # get all the `module_position` from `head_dataset`
        module_positions = head_dataset.module_position.unique()

        line_a = sorted([el for el in module_positions if 'A' in el])
        line_b = sorted([el for el in module_positions if 'B' in el])
        line_c = sorted([el for el in module_positions if 'C' in el])
        line_d = sorted([el for el in module_positions if 'D' in el])

        line_e = sorted([el for el in module_positions if 'E' in el])
        line_f = sorted([el for el in module_positions if 'F' in el])
        line_g = sorted([el for el in module_positions if 'G' in el])
        line_list = [line_a, line_b, line_c, line_d, line_e, line_f, line_g]
        
        module_position_summaryprod_partusage = []
        for i in  zip(summaryprod_partusage_df.line_name, summaryprod_partusage_df.module_number):
            try:
                # print(i)
                module_position_summaryprod_partusage.append(line_list[i[0]-1][i[1]-1])
            except:
                module_position_summaryprod_partusage.append(None)
                continue
                # warning
                # print(i, line_list)
                # print(line_list[i[0]-1][i[1]-1])
        # module_position_summaryprod_partusage = [line_list[i[0]-1][i[1]-1]  for i in  zip(summaryprod_partusage_df.line_name, summaryprod_partusage_df.module_number)]
        
        summaryprod_partusage_df['module_position'] = module_position_summaryprod_partusage
        
        # # sanity check : that the module_position are correctly named
        # (summaryprod_partusage_df.loc[:,'module_position'].apply(lambda x : x[-1])==summaryprod_partusage_df.loc[:,'Module'].apply(lambda x : x[-1])).value_counts()
        
        # replace '-' with 0
        summaryprod_partusage_df.replace('-', 0, inplace=True)
    
        # create columns

        temp_cols = ['Pickup count', 'Parts usage', 'Reject parts', 'No pickup', 'Error parts', 'Dislodged parts', 'Rescan count']#, 'Pickup rate', 'Reject rate', 'Error rate', 'Dislodged rate', 'Success rate']
        for col in temp_cols:
            head_dataset[col] = 0

        # map : increment the error count
        summaryprod_partusage_df_T = summaryprod_partusage_df.T.to_dict()
        for el in summaryprod_partusage_df_T.keys():
            module_position, date_time = summaryprod_partusage_df_T[el]['module_position'], datetime.strptime(summaryprod_partusage_df_T[el]['date_time'], '%Y-%m-%d %H:%M:%S')
            index = head_dataset[(head_dataset['module_position'] == module_position) & (head_dataset['start_dt']<date_time) & (date_time<=head_dataset['end_dt'])].index
        #     print(type(index))
            try:
                index = index[0]
            except:
        #         print(index)
                continue
            head_dataset.at[index, 'Pickup count'] += int(summaryprod_partusage_df_T[el]['Pickup count'])
            head_dataset.at[index, 'Parts usage'] += int(summaryprod_partusage_df_T[el]['Parts usage'])
            head_dataset.at[index, 'Reject parts'] += int(summaryprod_partusage_df_T[el]['Reject parts'])
            head_dataset.at[index, 'No pickup'] += int(summaryprod_partusage_df_T[el]['No pickup'])
            head_dataset.at[index, 'Error parts'] += int(summaryprod_partusage_df_T[el]['Error parts'])
            head_dataset.at[index, 'Dislodged parts'] += int(summaryprod_partusage_df_T[el]['Dislodged parts'])
            head_dataset.at[index, 'Rescan count'] += int(summaryprod_partusage_df_T[el]['Rescan count'])


        # summaryprod_production  - not mapped
        #summaryprod_production_df = pd.read_csv(collated_file_dir+'/summaryprod_production_collate.csv')


        # summaryprod_utilization  - not mapped
        # summaryprod_utilization_df = pd.read_csv(collated_file_dir+'/summaryprod_utilization_collate.csv')

        # write the modelling data set prepared from `base_schema` to csv
        head_dataset.to_csv(collated_file_dir+'/modelling_dataset.csv', index=False)

        logging.info("Success : Completed script 3b for {}".format(date_obj))
        return 0

    except Exception as e:
        logging.error("Failed : script 3b for {}".format(date_obj))
        logging.error(e)
        return(1, e)