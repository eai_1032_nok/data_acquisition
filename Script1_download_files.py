import os
import re
import json
from datetime import date, datetime, timedelta

# from azure.storage.blob import PublicAccess
from azure.storage.blob import BlockBlobService

import logging
# logger = logging.getLogger(__name__)


def download_blob_files(date_obj=None): # change remove default arg
    """
    date_obj : datetime.date object
    This function will download the blob files of the given date or past date 
    if date is empty"""
    try:
        logging.info("Running script 1 for {}".format(date_obj))
        assert isinstance(date_obj, date)
        base_path = './temp_production_blob_files_{}/'.format(date_obj.strftime('%Y%m%d'))
        
        # read config file
        with open("./config.json", "r") as f_pt:
            config = json.load(f_pt)

        # instantiate blob obj
        blob_account_name = config['blob_account_name']
        blob_account_key = config['blob_account_key']
        block_blob_service = BlockBlobService(blob_account_name, blob_account_key)

        # blob container list
        container_list = ['cycletime', 'errorinfoerrorline', 'errorinfoerrorstopage', \
        'errorinfopartdataerror', 'errorinfopartdataerrorfeeder', 'errorinfopartdataerrorholder', \
        'errorinfopartdataerrornozzle', 'errorinfopartsoutstopage', 'eventlog', 'floorlayout', \
        'moduleinformation', 'panel', 'partusagefidl', 'partusagepartnumber', 'partusageposition', \
        'summaryprodpartusage', 'summaryprodproduction', 'summaryprodutilization']

        # create {`container_name`: `files`} dict
        

        files_dict = {}

        for container_name in container_list:
            # do not get the following container files name
            if container_name in ['debugginglogs', 'extra']:
                continue
            logging.info("Downloading files of container - {}".format(container_name))
            files_dict[container_name] = []
            generator = block_blob_service.list_blobs(container_name)
            for blob in generator:
                # extract datetime from file name; date time convention in 
                # file names `20180827_133932`
                date_time_str = re.findall('\d{8}_\d{6}', blob.name)
                if len(date_time_str)==1:
                    date_time_str = date_time_str[0]
                else:
                    # file name does not contain the datetime stamp
                    continue
                #  filter the file names of the desired day
                if date_obj == datetime.strptime(date_time_str, '%Y%m%d_%H%M%S').date():
                    files_dict[container_name].append(blob.name)
            for container_name in files_dict.keys():
                dir_name = base_path+'/'+container_name
                os.makedirs(dir_name, exist_ok=True)
                # # to store the not downloaded files count
                # file_download_count = 0
                for el in files_dict[container_name]:
                    # if file_download_count>=5:
                    #     raise "Error while downloading raw `blob` file."
                    try:
                        local_file_name = el
                        full_path_to_file = dir_name+'/'+local_file_name

                        # if file already exists do not download it again
                        if os.path.isfile(full_path_to_file):
                            continue
                        block_blob_service.get_blob_to_path(container_name, local_file_name, full_path_to_file)
                    except Exception as e:
                        logging.warning('download_blob_files')
                        logging.warning(e)
                        # file_download_count += 1
        logging.info("Success : Completed script 1 for {}".format(date_obj))
        # to handle "script-down" case
        if len(files_dict['floorlayout'])==0:
            return 100
        return 0
    except Exception as e:
        logging.info("Failed : script 1 for {}".format(date_obj))
        return (1, e)
