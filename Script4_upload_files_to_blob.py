
# coding: utf-8

# ### This script will 
# - upload the collated files, base_schema and modelling_dataset to blob
# - delete the files downloaded from blob and above files from localsystem



import os
import shutil
import json
from datetime import date

from azure.storage.blob import PublicAccess
from azure.storage.blob import BlockBlobService
import logging
# logger = logging.getLogger(__name__)


def upload_to_container(block_blob_service, file_path, container_name):
    """This function will upload the file to container."""
    file_name = file_path.split('/')[-1]
    block_blob_service.create_blob_from_path(container_name, file_name, file_path)


def list_files(directory_path):
    """This function will return the list of the files in the provided directory."""
    if os.path.isdir(directory_path):
        return [directory_path+'/'+file_name for file_name in os.listdir(directory_path) if os.path.isfile(directory_path+'/'+file_name)]


def upload_to_blob(date_obj):
    """
    date_obj : datetime.date object
    """
    try:
        logging.info("Running : script 4 for {}".format(date_obj))
        assert isinstance(date_obj, date)

        # folder of floor_layout_collation (collated files)
        collated_file_dir = './collatedfiles'+date_obj.strftime('%Y%m%d')  # check the format

        # read config file
        with open("./config.json", "r") as f_pt:
            config = json.load(f_pt)

        # instantiate the objects, create a new container, and then set permissions on the container so the blobs are public
        blob_account_name = config['blob_account_name']
        blob_account_key = config['blob_account_key']
        block_blob_service = BlockBlobService(blob_account_name, blob_account_key)

        # #### create blob container name
        # - create container
        # - Set the permission so the blobs are public.

        container_name = 'collatedfiles'+date_obj.strftime('%Y%m%d')  # collatedfiles20180921  # check the container name prefix
        block_blob_service.create_container(container_name)
        block_blob_service.set_container_acl(container_name, public_access=PublicAccess.Container)


        files_to_upload = list_files(collated_file_dir)

        # upload files to container
        for file_path in files_to_upload:
            upload_to_container(block_blob_service, file_path, container_name)

        # delete files
        blob_files_folder = 'temp_production_blob_files_{}'.format(date_obj.strftime('%Y%m%d'))

        try:  # keep it in try because it should not stop the flow
            shutil.rmtree(blob_files_folder)  # check pass
            logging.info("Deleted the folder:'{}'".format(blob_files_folder))
            # pass
        except Exception as e:
            logging.info("Error while deleting the folder:'{}'".format(blob_files_folder))
            logging.error("{}".format(e))

        # do not delete the collated files  # check delete also but fetch the file from blob
        # try:
        #     shutil.rmtree(collated_blob_files_folder)
        # except Exception as e:
        #     logger.error("{}".format(e))

        logging.info("Success : Completed script 4 for {}".format(date_obj))
        return 0
    except Exception as e:
        logging.info("Failed : script 4 for {}".format(date_obj))
        return (1, e)

 