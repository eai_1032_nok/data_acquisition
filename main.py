
"""CHECKS
    
    if their is no data for a particular day the script stucks in loop
    BUG for date 2018-09-11

    Check main log
    check script 5 continue condition line 92

NAME = "{}-SEQ-{}-BATCH-{}".format(SEQ_LEN, BATCH_SIZE, int(time.time()))


data = df[df['head_id'].isin(head)]


    blob container collated files name convention : collatedfiles20180924
    folder naming convention
    temp_production_blob_files_'%Y%m%d'
    collatedfiles20180924
    
    put model pickle file in this directory
    

    APIs:
        script3a : actual positions
        script 6 : model predictionr result
    
"""

from script5_prepare_model_dataset import prepare_final_dataset 
from script6_run_model import run_model
from datetime import datetime
import logging

def main():
    log_filename = 'nokia_pm_{}.log'.format(datetime.now().date().strftime('%Y%m%d'))
    # log_filename = 'test.log'  # remove
    logging.basicConfig(filename=log_filename, level=logging.INFO, format='%(asctime)s %(levelname)s : %(message)s', datefmt='%d-%m-%Y %H:%M:%S')
    logging.info('\n\nStarted main()')

    # prepare dataset
    resp = prepare_final_dataset()
    if resp!=0:
        logging.error("Response : prepare_final_dataset")
        logging.error(resp)
        # raise

    # Run model
    resp = run_model()
    if resp!=0:
        logging.error("Response : run_model")
        logging.error(resp)
    logging.info('Finished main()')


if __name__=='__main__':
    main()
