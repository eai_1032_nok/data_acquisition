
# coding: utf-8

# ## This script will
# - This script will pick the collated `floorlayout` file and convert it to base scehma, in which data from other files will be mapped.
# - Its output will be saved in `collated_blob_files` as `base_schema.csv`



import pandas as pd
from datetime import datetime, timedelta, date
import json
import requests
import logging
# logger = logging.getLogger(__name__)


def prepare_base_schema(date_obj):
    """
    date_obj : datetime.date object
    """
    try:
        logging.info("Running : script 3a for {}".format(date_obj))
        assert isinstance(date_obj, date)

        # folder of floor_layout_collation (collated files)
        collated_file_dir = './collatedfiles'+date_obj.strftime('%Y%m%d')

        # read config file
        with open("./config.json", "r") as f_pt:
            config = json.load(f_pt)


        floor_layout_df = pd.read_csv(collated_file_dir+'/floor_layout_collation.csv')

        # convert date time format
        floor_layout_df['date_time'] = pd.to_datetime(floor_layout_df['date_time'], format='%Y-%m-%d %H:%M:%S')

        # drop empty columns and rows
        floor_layout_df.dropna(axis=1, how='all', inplace=True)
        floor_layout_df.dropna(axis=0, how='any', inplace=True)

        # drop '2M' data points
        # '2M' : Module for maintenance
        floor_layout_df = floor_layout_df[floor_layout_df['line_name']!='2M']

        # change the line name to numeric `1-7`
        floor_layout_df['line_name']  = floor_layout_df['line_name'].apply(lambda x : int(x[-1]))

        # change module_position naming convention
        floor_layout_df['module_position']  = floor_layout_df['module_position'].apply(lambda x :  x[:-9]+'__'+x[-1])

        # change module_name; remove braces
        # Module name will be Alphabet + number
        floor_layout_df['module_name'] = floor_layout_df['module_name'].apply(lambda x : x[:-7])  # re.sub("[^A-Za-z0-9]", '', 

        # sort along time stamp
        floor_layout_df.sort_values('date_time', inplace=True)

        # group and aggregate the data in desired df i.e;  `head_dataset`
        head_dataset = floor_layout_df.groupby(['module_position', 'head_id'])['date_time'].agg(['min', 'max']).reset_index()  # .to_csv('./dataset_preparation/prepared_dataset0.2.csv', index=False)  # .groupby('machine_id').count().reset_index().query('min>1')

        head_dataset.columns = ['module_position', 'head_id', 'min_dt', 'max_dt']
        head_dataset.sort_values(['module_position', 'min_dt'], ascending=True, inplace=True)

        # add the duration column( how long the module_position and head_id combination existed)
        head_dataset['duration_sec'] = (head_dataset['max_dt'] - head_dataset['min_dt'])#.dt.seconds

        # set the frequency (sec)
        frequency = timedelta(seconds=24*60*60)  # 1 day

        """
        This code adds the 'frequency' number of data points for a module_position and head_id pair and returns new dataframe.
        """
        data_points = []
        for el in head_dataset.T.to_dict().items():
            data = el[1]
            duration_sec = data['duration_sec']
            module_position = data['module_position']
            head_id = data['head_id']
            curr_dt_el = data['min_dt']

            # if the duration is less than or equal to frequency add the datapoint
            if duration_sec<=frequency:
                data_points.append({'duration_sec': frequency, 'head_id': data['head_id'],'end_dt': data['max_dt'], 'start_dt': data['min_dt'], 'module_position': data['module_position']})
        #         print(el[0])
                continue
        #     if duration is greater than frequency break it into datapoints
            while data['max_dt']-curr_dt_el>frequency:
                last_dt_el = curr_dt_el+frequency
                data_points.append({'duration_sec': frequency, 'head_id': data['head_id'],'end_dt': last_dt_el, 'start_dt': curr_dt_el, 'module_position': data['module_position']})
                curr_dt_el += frequency
            if data['max_dt']>curr_dt_el:
                data_points.append({'duration_sec': data['max_dt']-curr_dt_el, 'head_id': data['head_id'],'end_dt': data['max_dt'], 'start_dt': curr_dt_el, 'module_position': data['module_position']})

        # append all the dfs
        duration_df = pd.DataFrame(data_points)
        duration_df = duration_df.loc[:, ['module_position', 'head_id', 'start_dt', 'end_dt']]

        idx = duration_df.groupby(['module_position', 'head_id'])['end_dt'].transform('max')==duration_df['end_dt']#.loc[:,['end_dt']]#.reset_index()  # .agg('max')

        # mark every last dataset 1
        duration_df['Event'] = idx*1


        # for the latest pair of position mark them `0`
        idx = duration_df.groupby('module_position')['end_dt'].transform(max) == duration_df['end_dt']  # .value_counts()

        duration_df['Event'] = duration_df['Event'] *(~idx*1)#).value_counts()

        # duration_df['duration'] = (duration_df['end_dt']-duration_df['start_dt']).dt.total_seconds()

        # write to csv file
        duration_df.to_csv(collated_file_dir+'/base_schema.csv', index=False)

        """
        send the base schema to the API if date_obj is latest's date obj
        """
        if date_obj==(datetime.now()-timedelta(days=1)).date():
            # print(date_obj, (datetime.now()-timedelta(days=1)).date())
            logging.info("Trying to save the actual head position results to API.")
            url = config['actual_position_api']#'http://10.5.50.191:4000/api/v1/dashboard/createPosition'  # change

            # # data structure
            # data = {
            #   "positionsList": [
            #     {
            #       "machine": "string",
            #       "line": "string",
            #       "module": "string",
            #       "header_id": "string",
            #       "header_specification": "string",
            #       "date_time": "2018-09-26 23:34:35",  # end_dt
            #       "failureStatus": 0
            #     }
            #   ]
            # }
            duration_df['line'] = duration_df['module_position'].apply(lambda x : x[0])
            line_dict = {'A':1, 'B':2, 'C':3, 'D':4, 'E':5, 'F':6, 'G':7}
            duration_df['line'] = duration_df['line'].apply(lambda x : str(line_dict[x]))
            duration_df['module'] = duration_df['module_position'].apply(lambda x : x.split('__')[1])
            duration_df['machine'] = duration_df['module_position'].apply(lambda x : x.split('__')[0])
            # end datetime has been passed to API
            duration_df['date_time'] = duration_df['end_dt'].apply(lambda x : str(x))
            duration_df['header_id'] = duration_df['head_id']
            duration_df['failureStatus'] = duration_df['Event']
            duration_df['header_specification'] = ''

            data = duration_df.loc[:,["machine", "line", "module", "header_id", "header_specification", "date_time", "failureStatus"]].to_dict(orient='records')

            data = {"positionsList": data}
            logging.info('\n')
            logging.info(json.dumps(data))
            logging.info('\n')
            resp = requests.post(url, data=json.dumps(data), headers={"Content-Type":"application/json", "secretkey":"enrichai@nokia"})
            # print(12)
            if resp.status_code==200 and resp.text=='saved':
                logging.info("Saved the Actual position data to API.")
                logging.info(resp.status_code)
                logging.info(resp.text)
                return 0  # success
            else:
                logging.critical("API_Error while saving the Actual position data to API.")
                logging.critical(resp.status_code)
                logging.critical(resp.text)
                return 1
        logging.info("Success : Completed script 3a for {}".format(date_obj))
        return 0
    except Exception as e:
        logging.error("Failed : script 3a for {}".format(date_obj))
        logging.error(e)
        return (1, e)
