# coding: utf-8

# ### This script will transform the download data files from blob.
# 
# ### This script will not map the data to modelling dataset
# 
# ##### # upload the files to blob


import pandas as pd
from datetime import date, datetime, timedelta
import ast
import re
import os
from io import StringIO
import logging
# logger = logging.getLogger(__name__)


# cycletime - Not Mapped
def cycletime_func(base_path, collated_file_dir):
    try:
        logging.info("running cycletime_func")
        path = base_path+'/cycletime'
        files_list = os.listdir(path)
        
        # to map the line names Alphabet with numeric value
        d = {'A':1, 'B':2, 'C':3, 'D':4, 'E':5, 'F':6, 'G':7}

        # create data frame list for each file
        df_list = []
        for el in files_list:
            try:
                date, time = re.findall('\d{6,}', el)
                temp_df = pd.read_csv(path+'/'+el)
                temp_df['date_time'] = datetime.strptime(date+time, '%Y%m%d%H%M%S')
                temp_df['file_name'] = el
                if el[:4]=='2018':
                    temp_df['line_name'] = d[re.findall('@\w', el)[0][1]]
                    temp_df['lane_name'] = re.findall('Lane\d', el)[0]
                    temp_df['machine_name'] = re.findall('@[\w\-]{1,}', el)[0][1:]
                elif el[:4]=='line':
                    temp_df['line_name'] = el[4]
                    temp_df['lane_name'] = re.findall('Lane\d', el)[0]
                    temp_df['machine_name'] = re.findall('Lane\d[_\w]{1,}.', el)[0][6:-1]
                df_list.append(temp_df)
            except Exception as e:
                # file name does not contain datetime
                logging.warning("File Error : {}".format(el))
                logging.warning(e)
                continue

        cycletime_df = pd.concat(df_list, sort=False)
        del cycletime_df['Unnamed: 0']
        
        # write the data to csv file
        cycletime_df.to_csv(collated_file_dir+'/cycle_time_data.csv', index=False)
        logging.info("Succesfully ran cycletime_func")
        return 0

    except Exception as e:
        logging.error("Exception while running cycletime_func")
        logging.error(e)
        return (1, e)


# errorinfoerrorline - Mapped
def errorinfoerrorline_func(base_path, collated_file_dir):
    try:
        logging.info("running errorinfoerrorline_func")
        path = base_path + '/errorinfoerrorline'
        files_list = os.listdir(path)

        df_list = []
        for el in files_list:
            try:
                date, time = re.findall('\d{2,}', el)
                temp_df = pd.read_csv(path+'/'+el)
                temp_df['date_time'] = datetime.strptime(date+time, '%Y%m%d%H%M%S')
                temp_df['line'] = int(el[4:5])
                temp_df['file_name'] = el
                df_list.append(temp_df)
            except Exception as e:
                # file name does not contain datetime
                logging.warning("File Error : {}".format(el))
                logging.warning(e)
                continue
        errorinfoerrorline_df = pd.concat(df_list)
        
        # write to csv
        errorinfoerrorline_df.to_csv(collated_file_dir+'/error_info_errorline_collate.csv', index=False)
        logging.info("Succesfully ran errorinfoerrorline_func")
        return 0
        
    except Exception as e:
        logging.error("Exception while running errorinfoerrorline_func")
        logging.error(e)
        return (1, e)


# errorinfo errorrstopage
def errorinfoerrorstopage_func(base_path, collated_file_dir):
    try:
        logging.info("running errorinfoerrorstopage_func")
        path = base_path + '/errorinfoerrorstopage'
        files_list = os.listdir(path)

        df_list = []
        for el in files_list:
            try:
                date, time = re.findall('\d{2,}', el)
                temp_df = pd.read_csv(path+'/'+el)
                temp_df['date_time'] = datetime.strptime(date+time, '%Y%m%d%H%M%S')
                temp_df['line'] = int(el[4:5])
                temp_df['file_name'] = el
                df_list.append(temp_df)
            except Exception as e:
                # file name does not contain datetime
                logging.warning("File Error : {}".format(el))
                logging.warning(e)
                continue
        errorinfoerrorline_df = pd.concat(df_list)
        
        # write to csv
        errorinfoerrorline_df.to_csv(collated_file_dir+'/errorinfoerrorstopage_collate.csv', index=False)
        logging.info("Succesfully ran errorinfoerrorstopage_func")
        return 0
        
    except Exception as e:
        logging.error("Exception while running errorinfoerrorstopage_func")
        logging.error(e)
        return (1, e)


# errorinfo partdataerror  - Mapped
def errorinfopartdataerror_func(base_path, collated_file_dir):
    try:
        logging.info("running errorinfopartdataerror_func")
        path = base_path + '/errorinfopartdataerror/'
        files_list = os.listdir(path)

        df_list = []
        for file_name in files_list:
            try:
                date, time = re.findall('\d{2,}', file_name)
                text = ""
                with open(path+file_name, 'r') as f_pt:
                    data = f_pt.readlines()
                    if len(data)<=1:
                        # print("Empty File:{}".format(file_name))
                        continue
                    i=0
                    while i <len(data)-1:  # check '-1' condition
                        if data[i][-2] =='"':
                            text += data[i]
                            i += 1
                        else:
                            text += data[i].replace('\n', '')+data[i+1]
                            i+=2

            except Exception as e:
                # file name does not contain datetime
                logging.warning("File Error : {}".format(file_name))
                logging.warning(e)
                continue
            temp_df = pd.read_csv(StringIO(text))
            # if temp_df.empty: # check
            #     continue
            temp_df['date_time'] = datetime.strptime(date+time, '%Y%m%d%H%M%S')
            temp_df['line'] = int(file_name[4])
            temp_df['file_name'] = file_name
            df_list.append(temp_df)

        errorinfo_partdataerror_df = pd.concat(df_list, ignore_index=True)
        
        # write to csv
        errorinfo_partdataerror_df.to_csv(collated_file_dir+'/errorinfo_partdataerror.csv', index=False)
        logging.info("Succesfully ran errorinfopartdataerror_func")
        return 0
        
    except Exception as e:
        logging.error("Exception while running errorinfopartdataerror_func")
        logging.error(e)
        return (1, e)

# errorinfopartdata errorfeeder -Not mapped
def errorinfopartdataerrorfeeder_func(base_path, collated_file_dir):
    try:
        logging.info("running errorinfopartdataerrorfeeder_func")
        path = base_path + '/errorinfopartdataerrorfeeder/'
        files_list = os.listdir(path)

        df_list = []
        for file_name in files_list:
            try:
                date, time = re.findall('\d{2,}', file_name)

                # concat the data distributed across lines
                with open(path+file_name, 'r') as f_pt:
                    data = f_pt.readlines()
                if len(data)<=1:
                    # print("Empty File:{}".format(file_name))
                    continue
                i=0
                text = ""
                while i <len(data):
                    if data[i][-2] =='"' and data[i][0] =='"':
                        text += data[i]
                        i += 1
                    elif data[i][0] ==' ':
                        i += 1
                        continue
                    else:
                        text += data[i].replace('\n', '')+data[i+1]
                        i+=2

                lines = text.split('\n')
                text = ""
                for el in lines[1:]:
                    if el[:3]=='"",':
                        suffix = el
                        text += prefix+','+suffix+'\n'
                    elif len(el.split(',')[0])>2:
                        prefix = el

                # temp_df = pd.DataFrame(columns=["FIDL","Qty0","empty1","feeder_id","empty2","Qty","Error code","Cause"])
                temp_df = pd.read_csv(StringIO(text), header=None, names=["FIDL","Qty0","empty1","feeder_id","empty2","Qty","Error code","Cause"])
                if temp_df.empty:
                    continue
                temp_df['date_time'] = datetime.strptime(date+time, '%Y%m%d%H%M%S')
                temp_df['line'] = re.findall('Line_\d', file_name)[0][-1]
                temp_df['file_name'] = file_name
                df_list.append(temp_df)

            except Exception as e:
                # file name does not contain datetime
                logging.warning("File Error : {}".format(file_name))
                logging.warning(e)
                continue

        errorinfo_partdataerror_feeder_df = pd.concat(df_list, ignore_index=True, names=["FIDL","Qty0","empty1","feeder_id","empty2","Qty","Error code","Cause"])
        
        # write to csv
        errorinfo_partdataerror_feeder_df.to_csv(collated_file_dir+'/partdataerror_feeder.csv', index=False)
        logging.info("Succesfully ran errorinfopartdataerrorfeeder_func")
        return 0
        
    except Exception as e:
        logging.info("Exception while running errorinfopartdataerrorfeeder_func")
        logging.error(e)
        return (1, e)


# Error info part data error holder - Mapped
def errorinfopartdataerrorholder_func(base_path, collated_file_dir):
    try:
        logging.info("running errorinfopartdataerrorholder_func")
        path = base_path +'/errorinfopartdataerrorholder/'
        files_list = os.listdir(path)

        df_list = []
        for file_name in files_list:
            try:
                date, time = re.findall('\d{2,}', file_name)

                # concat the data distributed across lines
                with open(path+file_name, 'r') as f_pt:
                    data = f_pt.readlines()
                if len(data)<=1:
                    # print("Empty File:{}".format(file_name))
                    continue
                i=0
                text = ""
                while i <len(data):
                    if data[i][-2] =='"' and data[i][0] =='"':
                        text += data[i]
                        i += 1
                    elif data[i][0] ==' ':
                        i += 1
                        continue
                    else:
                        text += data[i].replace('\n', '')+data[i+1]
                        i+=2

                lines = text.split('\n')
                text = ""
                for el in lines[1:]:
                    if el[:3]=='"",':
                        suffix = el
                        text += prefix+','+suffix+'\n'
                    elif len(el.split(',')[0])>2:
                        prefix = el

            #     temp_df = pd.DataFrame(columns=["FIDL","Qty0","empty1","feeder_id","empty2","Qty","Error code","Cause"])
                temp_df = pd.read_csv(StringIO(text), header=None, names=["Holder","Qty0","empty1","feeder_id","empty2","Qty","Error code","Cause"])
                if temp_df.empty:
                    continue
                temp_df['date_time'] = datetime.strptime(date+time, '%Y%m%d%H%M%S')
                temp_df['line'] = re.findall('Line_\d', file_name)[0][-1]
                temp_df['file_name'] = file_name
                df_list.append(temp_df)

            except Exception as e:
                # file name does not contain datetime
                logging.warning("File Error : {}".format(file_name))
                logging.warning(e)
                continue

        errorinfo_partdataerror_holder_df = pd.concat(df_list, ignore_index=True)
        
        # write to csv
        errorinfo_partdataerror_holder_df.to_csv(collated_file_dir+'/errorinfo_partdataerror_holder_collate.csv', index=False)
        logging.info("Succesfully ran errorinfopartdataerrorholder_func")
        return 0
        
    except Exception as e:
        logging.info("Exception while running errorinfopartdataerrorholder_func")
        logging.error(e)
        return (1, e)


# Error info part data error nozzle -Mapped
def errorinfopartdataerrornozzle_func(base_path, collated_file_dir):
    try:
        logging.info("running errorinfopartdataerrornozzle_func")
        path = base_path +'/errorinfopartdataerrornozzle/'
        files_list = os.listdir(path)
        df_list = []
        for file_name in files_list:  #['line1_Information_20180820_014532_Part_data_Error_Nozzle_Line_1.csv']:#
            try:
                date, time = re.findall('\d{2,}', file_name)

                # concat the data distributed across lines
                with open(path+file_name, 'r') as f_pt:
                    data = f_pt.readlines()
                if len(data)<=1:
                    # print("Empty File:{}".format(file_name))
                    continue
                i=0
                text = ""
                while i <len(data):
                    if data[i][-2] =='"' and data[i][0] =='"':
                        text += data[i]
                        i += 1
                    elif data[i][0] ==' ':
                        i += 1
                        continue
                    else:
                        text += data[i].replace('\n', '')+data[i+1]
                        i+=2

                lines = text.split('\n')
                text = ""
                for el in lines[1:]:
                    if el[:3]=='"",':
                        suffix = el
                        text += prefix+','+suffix+'\n'
                    elif len(el.split(',')[0])>2:
                        prefix = el

                temp_df = pd.read_csv(StringIO(text), header=None, names=["nozzle_id","name","qty","empty1","feeder_id","empty2","empty3","qty2","Error code","Cause"])
                if temp_df.empty:
                    continue
                temp_df['date_time'] = datetime.strptime(date+time, '%Y%m%d%H%M%S')
                temp_df['line'] = re.findall('Line_\d', file_name)[0][-1]
                temp_df['file_name'] = file_name
                df_list.append(temp_df)

            except Exception as e:
                # file name does not contain datetime
                logging.warning("File Error : {}".format(file_name))
                logging.warning(e)
                continue

        errorinfo_partdataerror_nozzle_df = pd.concat(df_list, ignore_index=True)
        
        # write to csv
        errorinfo_partdataerror_nozzle_df.to_csv(collated_file_dir+'/errorinfo_partdataerror_nozzle_collate.csv', index=False)
        logging.info("Succesfully ran errorinfopartdataerrornozzle_func")
        return 0
        
    except Exception as e:
        logging.info("Exception while running errorinfopartdataerrornozzle_func")
        logging.error(e)
        return (1, e)


# errorinfopartsoutstopage - Mapped
def errorinfopartsoutstopage_func(base_path, collated_file_dir):
    try:
        logging.info("running errorinfopartsoutstopage_func")
        path = base_path +  '/errorinfopartsoutstopage'
        files_list = os.listdir(path)

        df_list = []
        for el in files_list:
            try:
                date, time = re.findall('\d{6,}', el)
                temp_df = pd.read_csv(path+'/'+el)
                temp_df['date_time'] = datetime.strptime(date+time, '%Y%m%d%H%M%S')
            #     temp_df['file_name'] = el
                if el[:4]=='2018':
                    temp_df['line_name'] = re.findall('Line-\d', el)[0][-1]
                elif el[:4]=='line':
                    temp_df['line_name'] = el[4]
                df_list.append(temp_df)
            except Exception as e:
                # file name does not contain datetime
                logging.warning("File Error : {}".format(el))
                logging.warning(e)
                continue

        errorinfopartsoutstopage_df = pd.concat(df_list)
        
        # write to csv
        errorinfopartsoutstopage_df.to_csv(collated_file_dir+'/errorinfopartsoutstopage_collate.csv', index=False)
        logging.info("Succesfully ran errorinfopartsoutstopage_func")
        return 0
        
    except Exception as e:
        logging.info("Exception while running errorinfopartsoutstopage_func")
        logging.error(e)
        return (1, e)


# eventlog - Mapped
def eventlog_func(base_path, collated_file_dir):
    try:
        logging.info("running eventlog_func")
        path = base_path +'/eventlog/'
        files_list = os.listdir(path)

        df_list = []
        for el in files_list:
        #     print(el)
            try:
                date, time = re.findall('\d{6,}', el)
                temp_df = pd.read_csv(path+'/'+el)
            #     temp_df['date_time'] = datetime.strptime(date+time, '%Y%m%d%H%M%S')
            #     temp_df['file_name'] = el
                if el[:4]=='2018':
                    temp_df['line_name'] = re.findall('Line-\d', el)[0][-1]
                elif el[:4]=='line':
                    temp_df['line_name'] = el[4]
                df_list.append(temp_df)
            except Exception as e:
                # file name does not contain datetime
                logging.warning("File Error : {}".format(el))
                logging.warning(e)
                continue

        eventlog_df = pd.concat(df_list)
        
        # write data to csv file
        eventlog_df.to_csv(collated_file_dir+'/eventlog_collate.csv', index=False)
        logging.info("Succesfully ran eventlog_func")
        return 0
        
    except Exception as e:
        logging.info("Exception while running eventlog_func")
        logging.error(e)
        return (1, e)


# Floor Layout
def floorlayout_func(base_path, collated_file_dir):
    try:
        logging.info("running floorlayout_func")
        # naming convention of file : lineNone_FloorLayout_20180820_014228.txt
        # entire data will be written in csv format in file 'floor_layout_collation.csv
        
        # dir path of files
        path = base_path + '/floorlayout'
        files_list = os.listdir(path)

        floor_layout_pt = open(collated_file_dir+'/floor_layout_collation.csv', 'w+')
        for file_name in files_list:
            try:
                data = None
                with open(path+'/{}'.format(file_name), 'r') as f_pt:
                    data = f_pt.read()
                if len(data)==0:
                    print(file_name)
                    continue
                data = ast.literal_eval(data)
                date, time = re.findall('\d{2,}', file_name)
                date_time = datetime.strptime(date+time, '%Y%m%d%H%M%S')
                machine_i = -1
                for el in data:
                    el0 = el[0].split(' ')  # line number and machine names
                    line_name, machines_names = el0[0], el0[1:]

                    ip_list = el[1]

                    module_ids = el[2]  # module ids
                    module_i = 0
                    machine_i = -1
                    for head in el[3]:
                        try:
                            if module_ids[module_i].split('\n')[0] == '1':
                                machine_i += 1
                            module_i += 1
                            module_id = module_ids[module_i-1].split('\n')[1]
                            module_position = machines_names[machine_i]+'_'+module_ids[module_i-1].split('\n')[0]
                            header_type, header_name, version = head.split('\n')  # header type, header name, version
            #                 print(el)
                        except Exception as e:
                            header_type, header_name, version, module_id, module_position = '', '', '', '', ''
            #                 print(e)
            #                 print(el[0], head)
                        finally:
            #                 print(machine_i, machines_names)
                            msg = "{},{},{},{},{},{},{}\n".format(line_name, machines_names[machine_i], date_time, header_type, header_name, module_id,module_position)
            #                 print(msg)
                            floor_layout_pt.write(msg)
            except Exception as e:
                # file name does not contain datetime
                logging.warning("File Error : {}".format(file_name))
                logging.warning(e)
                continue

        floor_layout_pt.flush()
        floor_layout_pt.close()
        floor_layout_df = pd.read_csv(collated_file_dir+'/floor_layout_collation.csv', header=None, names=['line_name', 'module_name', 'date_time', 'head_type', 'head_id', 'module_id', 'module_position'])
        floor_layout_df.to_csv(collated_file_dir+'/floor_layout_collation.csv', index=False)
        logging.info("Succesfully ran floorlayout_func")
        return 0
        
    except Exception as e:
        logging.info("Exception while running floorlayout_func")
        logging.error(e)
        return (1, e)


# module information
def moduleinformation_func(base_path, collated_file_dir):
    try:
        logging.info("running moduleinformation_func")
        path = base_path +'/moduleinformation/'
        files_list = os.listdir(path)

        # naming convention of file : lineNone_FloorLayout_20180820_014228.txt
        # entire data will be written in csv format in file 'floor_layout_collation.csv
        collated_file_path = collated_file_dir+'/module_information_collation.csv'
        module_information_pt = open(collated_file_path, 'w+')
        for file_name in files_list:
            try:
            #     print(file_name)
                data = None

                with open(path+'{}'.format(file_name), 'r') as f_pt:
                    data = f_pt.read()
                
                data = ast.literal_eval(data)
                date, time = re.findall('\d{2,}', file_name)
                date_time = datetime.strptime(date+time, '%Y%m%d%H%M%S')
                for line in data.keys():
                    if isinstance(data[line], list):  # for unstruuctured files
                        continue
                    machine_ids = data[line].keys()
                    for machine in machine_ids:
                        for station in data[line][machine]:
                            els = station.split('\n')
                            module_number, module_version, head_type, station_name = els[0], els[1], els[2], els[3]
                            nozzles = []
                            [nozzles.extend(re.findall(r'\d [\w()-]{1,}', nozzle_el)) for nozzle_el in els[4:-1]]
                            msg = "{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(date_time, line, machine, module_number, module_version, head_type, station_name, nozzles)
            #                 print(msg)
                            module_information_pt.write(msg)
            except Exception as e:
                # file name does not contain datetime
                logging.warning("File Error : {}".format(file_name))
                logging.warning(e)
                continue

        module_information_pt.flush()
        module_information_pt.close()
        
        #### read from csv
        columns = ['date_time', 'line_name', 'machine_name', 'module_number', 'module_version', 'head_type', 'station_name', 'nozzles']
        module_information_df = pd.read_csv(collated_file_path, header=None, names=columns, sep='\t')  # Check
        # module_information_df.shape

        # write to csv
        module_information_df.to_csv(collated_file_dir+'/module_information_collation.csv', index=False)
        logging.info("Succesfully ran moduleinformation_func")
        return 0
        
    except Exception as e:
        logging.info("Exception while running moduleinformation_func")
        logging.error(e)
        return (1, e)


# Panel
def panel_func(base_path, collated_file_dir):
    try:
        logging.info("running panel_func")
        path = base_path + '/panel/'
        files_list = os.listdir(path)

        df_list = []
        for file_name in files_list:
            try:
                date, time = re.findall('\d{2,}', file_name)

                # concat the data distributed across lines
                with open(path+file_name, 'r') as f_pt:
                    data = f_pt.readlines()
                if len(data)<=1:
                    # print(file_name)
                    # file size is zero
                    continue
                    # print(file_name)

                text = ""
                for line in data[1:]:  # skip the header present in the raw csv
                    if line[:3]=='"",':
                        suffix = line
                        text += prefix.replace('\n','')+','+suffix
                    elif len(line.split(',')[0])>2:
                        prefix = line
            #     print(text)
                if file_name[:4]=='2018':
                    try:
                        line_name = re.findall('Line-\d', file_name)[0]
                        lane_name = re.findall('Lane\d', file_name)[0]
                    except:
                        lane_name = "ALL"
                if file_name[:4]=='line':
                    try:
                        line_name = re.findall('line\d', file_name)[0]
                        lane_name = re.findall('Lane\d', file_name)[0]
                    except:
                        lane_name = "ALL"

                temp_df = pd.read_csv(StringIO(text), header=None, names=["job","panel","final_end_time","empty0","empty1","serial_no","individual_end_time","duration_s"])
                if temp_df.empty:
                    continue
                temp_df['line_name'] = line_name
                temp_df['lane_name'] = lane_name
                temp_df['date_time'] = datetime.strptime(date+time, '%Y%m%d%H%M%S')
                temp_df['file_name'] = file_name
                df_list.append(temp_df)
            except Exception as e:
                # file name does not contain datetime
                logging.warning("File Error : {}".format(file_name))
                logging.warning(e)
                continue

        panel_df = pd.concat(df_list)
        
        # write to csv
        panel_df.to_csv(collated_file_dir+'/panel_files_collation.csv', index=False)
        logging.info("Succesfully ran panel_func")
        return 0
        
    except Exception as e:
        logging.info("Exception while running panel_func")
        logging.error(e)
        return (1, e)


# Part usage FIDL
def partusagefidl_func(base_path, collated_file_dir):
    try:
        logging.info("running partusagefidl_func")
        path = base_path + '/partusagefidl/'
        files_list = os.listdir(path)

        df_list = []
        for el in files_list:
        #     print(el)
            try:
                date, time = re.findall('\d{6,}', el)
                temp_df = pd.read_csv(path+'/'+el)
                temp_df['date_time'] = datetime.strptime(date+time, '%Y%m%d%H%M%S')
                temp_df['line_name'] = el[4]
                df_list.append(temp_df)
            except Exception as e:
                # file name does not contain datetime
                logging.warning("File Error : {}".format(el))
                logging.warning(e)
                continue

        partusage_fidl_df = pd.concat(df_list)
        partusage_fidl_df.to_csv(collated_file_dir+'/partusage_fidl_collate.csv', index=False)
        logging.info("Succesfully ran partusagefidl_func")
        return 0
        
    except Exception as e:
        logging.info("Exception while running partusagefidl_func")
        logging.error(e)
        return (1, e)


# Part usage `partnumber`
def partusagepartnumber_func(base_path, collated_file_dir):
    try:
        logging.info("running partusagepartnumber_func")
        path = base_path + '/partusagepartnumber/'
        files_list = os.listdir(path)

        df_list = []
        for el in files_list:
        #     print(el)
            try:
                date, time = re.findall('\d{6,}', el)

                temp_df = pd.read_csv(path+'/'+el)
                temp_df['date_time'] = datetime.strptime(date+time, '%Y%m%d%H%M%S')
                temp_df['line_name'] = el[4]
                df_list.append(temp_df)
            except Exception as e:
                # file name does not contain datetime
                logging.warning("File Error : {}".format(el))
                logging.warning(e)
                continue

        partusage_partnumber_df = pd.concat(df_list)
        
        # write data to csv file
        partusage_partnumber_df.to_csv(collated_file_dir+'/partusage_partnumber_collate.csv', index=False)
        logging.info("Succesfully ran partusagepartnumber_func")
        return 0
        
    except Exception as e:
        logging.info("Exception while running partusagepartnumber_func")
        logging.error(e)
        return (1, e)


# Part usage `position`
def partusageposition_func(base_path, collated_file_dir):
    try:
        logging.info("running partusageposition_func")
        path = base_path + '/partusageposition/'
        files_list = os.listdir(path)

        df_list = []
        for el in files_list:
            try:
                # print(el)
                date, time = re.findall('\d{6,}', el)
                temp_df = pd.read_csv(path+'/'+el)
                temp_df['date_time'] = datetime.strptime(date+time, '%Y%m%d%H%M%S')
                temp_df['line_name'] = el[4]
                df_list.append(temp_df)
            except Exception as e:
                # file name does not contain datetime
                logging.warning("File Error : {}".format(el))
                logging.warning(e)
                continue

        partusage_position_df = pd.concat(df_list)
        # write to csv file
        partusage_position_df.to_csv(collated_file_dir+'/partusage_position_collate.csv', index=False)
        logging.info("Succesfully ran partusageposition_func")
        return 0
        
    except Exception as e:
        logging.info("Exception while running partusageposition_func")
        logging.error(e)
        return (1, e)


# summary prod part usage - Mapped
def summaryprodpartusage_func(base_path, collated_file_dir):
    try:
        logging.info("running summaryprodpartusage_func")
        path = base_path + '/summaryprodpartusage/'
        files_list = os.listdir(path)

        df_list = []
        for el in files_list:
            try:
                # extract date time from file name
                date, time = re.findall('\d{6,}', el)
            #     temp_df_T = temp_df.T.reset_index()
            #     temp_df_T.columns = temp_df_T.iloc[0,:].values

                temp_df = pd.read_csv(path+'/'+el, header=None, index_col=0)
                temp_df_T = temp_df.T#.reset_index()
                if temp_df_T.empty:
                    # print("Empty file:{}".format(el))
                    continue
                # temp_df_T.columns = temp_df_T.iloc[0,:].values
                # temp_df_T.columns = temp_df_T.iloc[0,:]
                # temp_df
                # temp_df_T = 
                temp_df_T = temp_df_T[~(temp_df_T['Module']=='Total')]
                temp_df_T.index = list(range(1, temp_df_T.shape[0]+1))
                temp_df_T.reset_index(inplace=True)
                temp_df_T

                # rename column names
                temp_df_T.columns = ['module_number', 'Module', 'Pickup count', 'Parts usage', 'Reject parts', 'No pickup', 'Error parts', 'Dislodged parts', 'Rescan count', 'Pickup rate', 'Reject rate', 'Error rate', 'Dislodged rate', 'Success rate']


                # drop the row which is duplicate of column names
            #     temp_df_T.drop(0, inplace=True)

                # add date time
                temp_df_T['date_time'] = datetime.strptime(date+time, '%Y%m%d%H%M%S')
                # add line name/number
                temp_df_T['line_name'] = el[4]
                temp_df_T['file_name'] = el

                df_list.append(temp_df_T.copy())
            except Exception as e:
                # file name does not contain datetime
                logging.warning("File Error : {}".format(el))
                logging.warning(e)
                continue

        summaryprod_partusage_df = pd.concat(df_list, ignore_index=True)

        summaryprod_partusage_df.to_csv(collated_file_dir+'/summary_prod_partusage_collate.csv', index=False)
        logging.info("Succesfully ran summaryprodpartusage_func")
        return 0
        
    except Exception as e:
        logging.info("Exception while running summaryprodpartusage_func")
        logging.error(e)
        return (1, e)


# summary prod production - Not mapped
def summaryprodproduction_func(base_path, collated_file_dir):
    try:
        logging.info("running summaryprodproduction_func")
        path = base_path + '/summaryprodproduction/'
        files_list = os.listdir(path)
        df_list = []
        for el in files_list:
        #     print(el)
            try:
                date, time = re.findall('\d{6,}', el)

                temp_df = pd.read_csv(path+'/'+el)
                temp_df['date_time'] = datetime.strptime(date+time, '%Y%m%d%H%M%S')
                temp_df['line_name'] = el[4]
                temp_df['file_name'] = el
                df_list.append(temp_df)
            except Exception as e:
                # file name does not contain datetime
                logging.warning("File Error : {}".format(file_name))
                logging.warning(e)
                continue

        summaryprod_production_df = pd.concat(df_list, ignore_index=True)
        summaryprod_production_df.to_csv(collated_file_dir+'/summaryprod_production_collate.csv', index=False)
        logging.info("Succesfully ran summaryprodproduction_func")
        return 0
        
    except Exception as e:
        logging.info("Exception while running summaryprodproduction_func")
        logging.error(e)
        return (1, e)


# summary prod utilization -Not Mapped
def summaryprodutilization_func(base_path, collated_file_dir):
    try:
        logging.info("running summaryprodutilization_func")
        path = base_path + '/summaryprodutilization/'
        files_list = os.listdir(path)

        df_list = []
        for el in files_list:
            try:
                # extract date time from file name
                date, time = re.findall('\d{6,}', el)
                temp_df = pd.read_csv(path+'/'+el, header=None)
                temp_df_T = temp_df.T.reset_index()
            #     temp_df_T.columns = temp_df_T.iloc[0,:].values

                # drop the row which is duplicate of column names
                temp_df_T.drop(0, inplace=True)

                # rename column names
                temp_df_T.columns = ['module_number','Module','Product','Wait Previous','Wait Next','Changeover','Part Supply','Machine Error', 'Operator Downtime','Maintenance','Other']
                # add date time
                temp_df_T['date_time'] = datetime.strptime(date+time, '%Y%m%d%H%M%S')
                # add line name/number
                temp_df_T['line_name'] = el[4]

                df_list.append(temp_df_T.copy())
            except Exception as e:
                # file name does not contain datetime
                logging.warning("File Error : {}".format(el))
                logging.warning(e)
                continue

        summaryprod_utilization_df = pd.concat(df_list, ignore_index=True)
        summaryprod_utilization_df.to_csv(collated_file_dir+'/summaryprod_utilization_collate.csv', index=False)
        logging.info("Succesfully ran summaryprodutilization_func")
        return 0
        
    except Exception as e:
        logging.info("Exception while running summaryprodutilization_func")
        logging.error(e)
        return (1, e)


def data_transformation(date_obj):
    """
    date_obj : datetime.date object
    """
    try:
        logging.info("Running : Script 2 for {}".format(date_obj))
        assert isinstance(date_obj, date)
        
        collated_file_dir = './collatedfiles'+date_obj.strftime('%Y%m%d')
        os.makedirs(collated_file_dir, exist_ok=True)
        base_path = './temp_production_blob_files_{}'.format(date_obj.strftime('%Y%m%d'))

        resp = cycletime_func(base_path, collated_file_dir)
        if resp!=0:
            logging.error("cycletime_func")
            logging.error(resp)
        resp = errorinfoerrorline_func(base_path, collated_file_dir)
        if resp!=0:
            logging.error("errorinfoerrorline_func")
            logging.error(resp)
        resp = errorinfoerrorstopage_func(base_path, collated_file_dir)
        if resp!=0:
            logging.error("errorinfoerrorstopage_func")
            logging.error(resp)
        resp = errorinfopartdataerror_func(base_path, collated_file_dir)
        if resp!=0:
            logging.error("errorinfopartdataerror_func")
            logging.error(resp)
        resp = errorinfopartdataerrorfeeder_func(base_path, collated_file_dir)
        if resp!=0:
            logging.error("errorinfopartdataerrorfeeder_func")
            logging.error(resp)
        resp = errorinfopartdataerrorholder_func(base_path, collated_file_dir)
        if resp!=0:
            logging.error("errorinfopartdataerrorholder_func")
            logging.error(resp)
        resp = errorinfopartdataerrornozzle_func(base_path, collated_file_dir)
        if resp!=0:
            logging.error("errorinfopartdataerrornozzle_func")
            logging.error(resp)
        resp = errorinfopartsoutstopage_func(base_path, collated_file_dir)
        if resp!=0:
            logging.error("errorinfopartsoutstopage_func")
            logging.error(resp)
        resp = eventlog_func(base_path, collated_file_dir)
        if resp!=0:
            logging.error("eventlog_func")
            logging.error(resp)
        resp = floorlayout_func(base_path, collated_file_dir)
        if resp!=0:
            logging.error("floorlayout_func")
            logging.error(resp)
        resp = moduleinformation_func(base_path, collated_file_dir)
        if resp!=0:
            logging.error("moduleinformation_func")
            logging.error(resp)
        resp = panel_func(base_path, collated_file_dir)
        if resp!=0:
            logging.error("base_path")
            logging.error(resp)
        resp = partusagefidl_func(base_path, collated_file_dir)
        if resp!=0:
            logging.error("partusagefidl_func")
            logging.error(resp)
        resp = partusagepartnumber_func(base_path, collated_file_dir)
        if resp!=0:
            logging.error("partusagepartnumber_func")
            logging.error(resp)
        resp = partusageposition_func(base_path, collated_file_dir)
        if resp!=0:
            logging.error("partusageposition_func")
            logging.error(resp)
        resp = summaryprodpartusage_func(base_path, collated_file_dir)
        if resp!=0:
            logging.error("summaryprodpartusage_func")
            logging.error(resp)
        resp = summaryprodproduction_func(base_path, collated_file_dir)
        if resp!=0:
            logging.error("summaryprodproduction_func")
            logging.error(resp)
        resp = summaryprodutilization_func(base_path, collated_file_dir)
        if resp!=0:
            logging.error("summaryprodutilization_func")
            logging.error(resp)
        logging.info("Success : Completed script 2 for {}".format(date_obj))
        return 0
    except Exception as e:
        logging.info("Failed : script 2 for {}".format(date_obj))
        return (1, e)