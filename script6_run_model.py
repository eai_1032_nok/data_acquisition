
# coding: utf-8

# #### This script will 
# - run the Logistic Regression model on the dataset of scritp 5
# - save the result through API

# In[28]:


import pandas as pd
# import numpy as np
# import pickle
import json
from datetime import datetime
# from sklearn.model_selection import train_test_split
# from sklearn.linear_model import LogisticRegression
# from sklearn.metrics import classification_report, confusion_matrix
import logging
import requests
import tensorflow as tf
from tensorflow.keras.models import load_model
from tensorflow.keras import backend as K 

import EnrichAI_PM_RNN.train_model as train_func



def run_model():
    try:
        logging.info("Running : Starting script 6")

        # read config file
        with open("./config.json", "r") as f_pt:
            config = json.load(f_pt)

        # load dataset
        dataset = pd.read_csv('./final_dataset.csv')
        # print(dataset.shape)

        """LR
        with open('LR_model.pck', 'rb') as f_pt:
            clf_model = pickle.load(f_pt)
        predicted_result = clf_model.predict_proba(dataset)
        print(predicted_result)
        """

        
        logging.info("Initiating model")

        MODEL_NAME = './EnrichAI_PM_RNN/models/10-SEQ-10-BATCH-1538723763.h5'

        df = pd.read_csv("./final_dataset.csv")
        df = train_func.prepare_test(df)

        heads = df.head_id.unique()

        final_result_0 = []
        final_result_1 = []
        # print(10)
        K.clear_session()
        logging.info("Session Cleared")
        model = load_model(MODEL_NAME)
        logging.info("Model loaded")
        for head in heads:
            test_X = train_func.preprocess_test(df, head)
            if len(test_X)==0:
                result_0 = 1
                result_1 = 0
            else:
                result = model.predict(test_X)
                result_0 = result[0][0]
                result_1 = result[0][1]

            final_result_0.append(result_0)
            final_result_1.append(result_1)


        final_df = pd.DataFrame()
        final_df['head'] = heads
        final_df['result_0'] = final_result_0
        final_df['result_1'] = final_result_1

        logging.info("Result Recieved")
        # print(11)

        # result_df = pd.DataFrame(predicted_result)

        # logging.info("saving result to API")
        
        # save the predicted result througth API
        url = config['prediction_result_api']  # 'http://10.5.50.191:4000/api/v1/dashboard/predictedResults'

        # # convert the predicted result in the required structure
        # data = {
        #   "predictedList": [
        #     {
        #       "header_id": "string",
        #       "model_run_date": "2018-09-26 23:34:35",
        #       "model": "string",
        #       "predicted_value": 0,
        #       "status": "string",
        #       "bin_l": 0,
        #       "bin_h": 0
        #     }
        #   ]
        # }

        final_df['header_id'] = final_df['head']
        final_df['model_run_date'] = str(datetime.now())
        final_df['model'] = "RNN"
        final_df['predicted_value'] = final_df['result_1'].round(5)

        final_df['bin_l'] = final_df['bin_h'] = 0

        final_df['status'] = final_df['status'] = ['critical' if x>0.8 else ('needs_attention' if x>0.5 else 'working_fine') for x in final_df['predicted_value']]

        final_df.at[final_df['status']=='working_fine','bin_l'] = 0.0
        final_df.at[final_df['status']=='working_fine','bin_h'] = 0.5
        final_df.at[final_df['status']=='needs_attention','bin_l'] = 0.5
        final_df.at[final_df['status']=='needs_attention','bin_h'] = 0.8
        final_df.at[final_df['status']=='critical','bin_l'] = 0.8
        final_df.at[final_df['status']=='critical','bin_h'] = 1.0

        cols = ["header_id", "model_run_date", "model", "predicted_value", "status", "bin_l", "bin_h"]
        data = final_df.loc[:,cols].to_dict(orient='records')

        # save to file
        prediction_filename = 'predictions_{}.log'.format(datetime.now().date().strftime('%Y%m%d'))
        final_df.loc[:,cols].to_csv(prediction_filename, index=False)

        data = {"predictedList": data}
        logging.info('\n')
        logging.info(json.dumps(data))
        logging.info('\n')
        resp = requests.post(url, data=json.dumps(data), headers={"Content-Type":"application/json", "secretkey":"enrichai@nokia"})
        if resp.status_code==200 and resp.text=='saved':
            logging.info("Saved the Prediction result to API.")
            logging.info(resp.status_code, resp.text)
            return 0  # success
        else:
            logging.critical("API_Error while saving the Prediction result to API.")
            logging.critical(resp.status_code, resp.text)
            return 1
        logging.info("Success : Completed script 6")
        return 0
    except Exception as e:
        logging.error("Failed : script 6")
        logging.error(e)
        return (1, e)
