# Allows you to launch/initialise a browser.
from selenium import webdriver

# Allows you to search for things using specific parameters.
from selenium.webdriver.common.by import By

# Allows you to wait for a page to load.
from selenium.webdriver.support.ui import WebDriverWait

# Specify what you are looking for on a specific page in order to determine that the webpage has loaded.
from selenium.webdriver.support import expected_conditions as EC

# Handling a timeout situation.
from selenium.common.exceptions import TimeoutException

import pandas as pd

# Create new instance of Chrome
option = webdriver.ChromeOptions()

# # for incognito mode
# option.add_argument(" - incognito")


# create a new instance of Chrome
browser = webdriver.Chrome(executable_path='./chromedriver', chrome_options=option)


# hit the url
# browser.get("https://github.com/TheDancerCodes")
# browser.get("https://money.rediff.com/indices")
browser.get("http://tm-dashboard.eu-gb.mybluemix.net")


# to verify the login page has loaded fully

# Wait 20 seconds for page to load
timeout = 20
try:
    # We pass in the <img> tag and its class to the WebDriverWait() function as the XPATH in the code snippet above
    WebDriverWait(browser, timeout).until(EC.visibility_of_element_located((By.XPATH, "//img[@src='img/unlimit-logo.png']")))

except TimeoutException:
    print("Timed out waiting for page to load")
    browser.quit()  # do not quit the browser


# sign in
username = browser.find_element_by_xpath("//input[@name='username']")
username.clear()
password = browser.find_element_by_xpath("//input[@name='password']")
password.clear()

# it is throwing error
# login_btn = browser.find_element_by_xpath("//div/button[@class='btn btn-primary btnlogin']")
# login_btn = browser.find_element_by_xpath("//div[@class='input-group padt20']")

username.send_keys('BYPL_Admin')
password.send_keys('unlimit@123')


# login_btn.click()
login_btn = browser.find_element_by_class_name('btnlogin')
browser.execute_script("arguments[0].click();", login_btn)













# ----------------------------------------- #

# After logging in




timeout = 20
try:
    # # # table head
    # WebDriverWait(browser, timeout).until(EC.visibility_of_element_located((By.XPATH, "//table")))
    # table = browser.find_element_by_xpath("//table[@ng-table='tableParams']")
    # # titles = [x.text for x in table]
    # print(table.text)

    # # table body
    # WebDriverWait(browser, timeout).until(EC.visibility_of_element_located((By.XPATH, "//tbody")))
    # tbody = browser.find_element_by_xpath("//tbody")
    # # titles = [x.text for x in table]
    # print(tbody.text)

    WebDriverWait(browser, timeout).until(EC.visibility_of_element_located((By.XPATH, "//tr")))
    table = browser.find_elements_by_xpath("//tr")
    titles = [x.text for x in [t for t in table]]
    print(titles)
except TimeoutException:
    print("Timed out waiting for page to load")
    # browser.quit()  # do not quit the browser




# find_elements_by_xpath returns an array of selenium objects.
# We pass in the <a> tag and its class to the find_elements_by_xpath() function in the code snippet above


# find the "show more element" to click
# show_more = browser.find_elements_by_xpath("//a[@id='showMoreLess']")

# browser.find_elements_by_id("showMoreLess").click()

# table = browser.find_elements_by_xpath("//table[@class='dataTable']")





# titles = [x.text for x in table]
# print(titles)





# use list comprehension to get the actual repo titles and not the selenium objects.
# print(type(table))
# print(len(table))
# print(table)
# table_elements = [x for x in table]

# print(table_elements)





# # We will now get all the languages for the pinned repositories.
# language_element = browser.find_elements_by_xpath("//p[@class='mb-0 f6 text-gray']")

# # same concept as for list-comprehension above.
# languages = [x.text for x in language_element]

# print("languages:")
# print(languages, '\n')



# # Combine the responses using zip function
# for title, language in zip(titles, languages):
#     print("RepoName : Language")
#     print(title + ": " + language, '\n')
#


